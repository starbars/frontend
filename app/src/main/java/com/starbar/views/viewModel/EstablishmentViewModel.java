package com.starbar.views.viewModel;

import android.graphics.Bitmap;

import com.starbar.domain.centerPoint.Coordinates;

import java.util.ArrayList;
import java.util.List;

public class EstablishmentViewModel {
    private Coordinates coordinates;
    private String name;
    private Bitmap placePhoto;
    private Double placeRate;
    private List<String> categories;

    public EstablishmentViewModel(Coordinates coordinates, String name, Bitmap placePhoto, double placeRate) {
        this.coordinates = coordinates;
        this.name = name;
        this.placePhoto = placePhoto;
        this.placeRate = placeRate;
        this.categories = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getPlacePhoto() {
        return placePhoto;
    }

    public void setPlacePhoto(Bitmap cePhoto) {
        this.placePhoto = placePhoto;
    }

    public double getPlaceRate() {
        return placeRate;
    }

    public void setPlaceRate(Double placeRate) {
        this.placeRate = placeRate;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void addCategory(String category) {
        this.categories.add(category);
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "EstablishmentViewModel{" +
                "name='" + name + '\'' +
                ", placePhoto=" + placePhoto +
                ", placeRate=" + placeRate +
                ", categories=" + categories +
                '}';
    }
}
