package com.starbar.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.starbar.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity {

    EditText location_text1;
    EditText location_text2;
    EditText location_text3;
    Spinner establishment_spinner;
    Spinner results_number_spinner;
    TextView radius_value;
    SeekBar seekBar;

    String [] establishment_type_options = {"","bar", "restaurant", "cafe"};
    Integer [] results_number_options = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.app_logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        location_text1 = (EditText) findViewById(R.id.editText1);
        location_text2 = (EditText) findViewById(R.id.editText2);
        location_text3 = (EditText) findViewById(R.id.editText3);
        establishment_spinner = (Spinner) findViewById(R.id.establishment_spinner);
        results_number_spinner = (Spinner) findViewById(R.id.results_spinner);
        radius_value = (TextView) findViewById(R.id.radius_value);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int value_step = 50;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = ((int)Math.round(progress/value_step ))*value_step;
                seekBar.setProgress(progress);
                radius_value.setText(progress + "");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // This method will automatically
                // called when the user touches the SeekBar
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // This method will automatically
                // called when the user
                // stops touching the SeekBar
            }
        });

        ArrayAdapter<String> adapter_establishment_type = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,establishment_type_options);
        ArrayAdapter<Integer> adapter_results_number = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item,results_number_options);
        establishment_spinner.setAdapter(adapter_establishment_type);
        results_number_spinner.setAdapter(adapter_results_number);


        findViewById(R.id.triangulate_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> location_names = new ArrayList<>();
                location_names.add(location_text1.getText().toString());
                location_names.add(location_text2.getText().toString());
                location_names.add(location_text3.getText().toString());

                try {
                    triangulate(location_names);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    int value_step = 50;
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        progress = ((int)Math.round(progress/value_step ))*value_step;
        seekBar.setProgress(progress);
        radius_value.setText(progress + "");
    }

    private void triangulate(List<String> locations) throws IOException {

        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(
                        mediaType, "[" + "\"" + locations.get(0) + "\"" + ", " + "\"" + locations.get(1) + "\"" + ", " + "\"" + locations.get(2) + "\"" + "]");
                Request request = new Request.Builder()
                        .url("https://0ed5-2001-41d0-1-745b-00-1.eu.ngrok.io/places/search?results_number="
                                + results_number_spinner.getSelectedItem().toString()
                                + "&places_type=" + establishment_spinner.getSelectedItem().toString()
                                + "&radius_meters=" +
                                radius_value.getText().toString())
                        .method("POST", body)
                        .addHeader("Content-Type", "application/json")
                        .build();
                Response response = null;
                try {
                    response = client.newCall(request).execute();

                    if(response.code() == 200) {
                        Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                        intent.putExtra("json", response.body().string());
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Error found, try later", Toast.LENGTH_LONG).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}