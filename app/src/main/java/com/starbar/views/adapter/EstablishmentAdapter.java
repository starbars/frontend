package com.starbar.views.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.starbar.R;
import com.starbar.views.viewModel.EstablishmentViewModel;

import java.util.List;

public class EstablishmentAdapter extends RecyclerView.Adapter<EstablishmentAdapter.ViewHolder>{

    public interface OnEstablishmentListener {
        void onEstablishmentClick(int position);
    }

    private OnEstablishmentListener mOnEstablishmentListener;

    private List<EstablishmentViewModel> establishmentViewModelList;

    public EstablishmentAdapter(List<EstablishmentViewModel> establishmentViewModelList, OnEstablishmentListener mOnEstablishmentListener) {
        this.establishmentViewModelList = establishmentViewModelList;
        this.mOnEstablishmentListener = mOnEstablishmentListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.bar_row_layout,
                        parent,
                        false
                ) ;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bitmap imageBitmap = establishmentViewModelList.get(position).getPlacePhoto();
        holder.imageView.setImageBitmap(imageBitmap);
        holder.placeName.setText(establishmentViewModelList.get(position).getName());
        String rate = Double.toString(establishmentViewModelList.get(position).getPlaceRate());
        holder.placeRate.setText(rate + "/5");
        String category = "";
        for(int i = 0; i < establishmentViewModelList.get(position).getCategories().size(); i++) {
            String firstCapitalLetter = establishmentViewModelList.get(position).getCategories().get(i).substring(0, 1).toUpperCase();
            String remainingCategory = establishmentViewModelList.get(position).getCategories().get(i).substring(1);
            String capitalCategory = firstCapitalLetter + remainingCategory;
            String fixCategory = capitalCategory.replaceAll("_", " ");
            if (i == 0) {
                category = fixCategory;
            } else {
                category = category + ", " + fixCategory;
            }
        }
        holder.placeCategories.setText(category);
    }

    @Override
    public int getItemCount() {
        return establishmentViewModelList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        private ImageView imageView;
        private TextView placeName;
        private TextView placeRate;
        private TextView placeCategories;

        public ViewHolder(@NonNull View v) {
            super(v);
            v.setOnClickListener(this);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            placeName = (TextView) v.findViewById(R.id.place_name);
            placeRate = (TextView) v.findViewById(R.id.place_rate);
            placeCategories = (TextView) v.findViewById(R.id.place_categories);
        }


        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            mOnEstablishmentListener.onEstablishmentClick(pos);
        }
    }
}

