package com.starbar.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.Visibility;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.starbar.BuildConfig;
import com.starbar.R;
import com.starbar.domain.centerPoint.CenterPoint;
import com.starbar.domain.centerPoint.GooglePlace;
import com.starbar.domain.centerPoint.Coordinates;
import com.starbar.views.adapter.EstablishmentAdapter;
import com.starbar.views.viewModel.EstablishmentViewModel;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, EstablishmentAdapter.OnEstablishmentListener {

    List<EstablishmentViewModel> establishmentList = new ArrayList<>();
    private Coordinates centerPointCoords;
    private GoogleMap map;
    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.app_logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        Intent intent = getIntent();
        Gson gson = new Gson();
        Places.initialize(getApplicationContext(), BuildConfig.MAPS_API_KEY);
        PlacesClient placesClient = Places.createClient(this);

        CenterPoint centerPoint = gson.fromJson(intent.getStringExtra("json"), CenterPoint.class);

        centerPointCoords = centerPoint.getCoordinates();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TextView noPlacesText = (TextView) findViewById(R.id.no_places_text);

        if (centerPoint.getPlaces().size() == 0) {
            RecyclerView recyclerViewEstablishments = (RecyclerView) findViewById(R.id.bar_list);
            recyclerViewEstablishments.setVisibility(View.GONE);
            noPlacesText.setText("No places found in this area, try to increase the radius " +
                    "or change any location");
            return;
        }


        // Create list of establishments view model
        for (GooglePlace googlePlace : centerPoint.getPlaces()) {
            noPlacesText.setVisibility(View.GONE);

            String placeId = googlePlace.getId();

            List<Place.Field> fields = Collections.singletonList(Place.Field.PHOTO_METADATAS);

            FetchPlaceRequest placeRequest = FetchPlaceRequest.newInstance(placeId, fields);

            placesClient.fetchPlace(placeRequest).addOnSuccessListener((response) -> {
                Place place = response.getPlace();

                // Get metadata of the place, if it has a photo metadata
                List<PhotoMetadata> metadata = place.getPhotoMetadatas();
                if(metadata == null || metadata.isEmpty()) {
                    createEstablishmentModel(googlePlace, null);
                    return;
                }

                PhotoMetadata photoMetadata = metadata.get(0);

                FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photoMetadata)
                        .setMaxWidth(300)
                        .setMaxHeight(300)
                        .build();
                placesClient.fetchPhoto(photoRequest).addOnSuccessListener((fetchPhotoResponse -> {
                    createEstablishmentModel(googlePlace, fetchPhotoResponse.getBitmap());
                })).addOnFailureListener((exception) -> {
                    if(exception instanceof ApiException) {
                        final ApiException apiException = (ApiException) exception;
                        Log.e("ERROR FETCH PLACE", "Place not found: " + exception.getMessage());
                    }
                });
            });
        }
    }

    private void createEstablishmentModel(GooglePlace googlePlace, Bitmap photoBitmap) {
        EstablishmentViewModel establishment =
                new EstablishmentViewModel(
                        googlePlace.getLocation(),
                        googlePlace.getName(),
                        photoBitmap,
                        googlePlace.getRating()
                );

        establishment.addCategory(googlePlace.getTypes().get(0));

        if(googlePlace.getTypes().size() > 1) {
            establishment.addCategory(googlePlace.getTypes().get(1));
        }
        establishmentList.add(establishment);

        RecyclerView recyclerViewEstablishments = (RecyclerView) findViewById(R.id.bar_list);

        // to improve performance with changes in the content
        recyclerViewEstablishments.setHasFixedSize(true);

        // for nested scroll features
        recyclerViewEstablishments.setNestedScrollingEnabled(false);

        // linear layout manager
        recyclerViewEstablishments.setLayoutManager(new LinearLayoutManager((this)));

        // specify adapter with the list to show
        EstablishmentAdapter mAdapter = new EstablishmentAdapter(establishmentList, this);
        recyclerViewEstablishments.setAdapter(mAdapter);
    }


    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        this.map = googleMap;
        LatLng coordinates = new LatLng(centerPointCoords.getLatitude(), centerPointCoords.getLongitude());
        marker = googleMap.addMarker(new MarkerOptions()
                .position(coordinates)
                .title("Central point marker")
                .icon(BitmapDescriptorFactory.fromBitmap(resizeBitmap("map_marker", 150, 150))));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(coordinates));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    public Bitmap resizeBitmap(String drawableName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources()
                .getIdentifier(drawableName, "drawable", getPackageName()));
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    @Override
    public void onEstablishmentClick(int position) {
        EstablishmentViewModel establishmentSelected = establishmentList.get(position);
        Coordinates establishmentCoordinates = establishmentSelected.getCoordinates();
        LatLng coordinates = new LatLng(establishmentCoordinates.getLatitude(), establishmentCoordinates.getLongitude());
        marker.remove();
        marker = map.addMarker(new MarkerOptions()
                .position(coordinates)
                .title(establishmentSelected.getName())
                .icon(BitmapDescriptorFactory.fromBitmap(resizeBitmap("map_marker", 150, 150))));
        map.animateCamera(CameraUpdateFactory.newLatLng(coordinates));
    }
}