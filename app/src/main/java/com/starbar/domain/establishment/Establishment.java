package com.starbar.domain.establishment;

public class Establishment {

    private String name;
    private float score;
    private String category;

    public Establishment(String name, float score, String category) {
        this.name = name;
        this.score = score;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
