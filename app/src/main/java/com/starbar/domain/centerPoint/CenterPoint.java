package com.starbar.domain.centerPoint;

import java.util.List;

public class CenterPoint {

    private Coordinates center_location;
    private List<GooglePlace> places;

    public CenterPoint(Coordinates coordinates, List<GooglePlace> places) {
        this.center_location = coordinates;
        this.places = places;
    }

    public Coordinates getCoordinates() {
        return center_location;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.center_location = coordinates;
    }

    public List<GooglePlace> getPlaces() {
        return places;
    }

    public void setPlaces(List<GooglePlace> googlePlaces) {
        this.places = googlePlaces;
    }

    @Override
    public String toString() {
        return "CenterPoint{" +
                "coordinates=" + center_location +
                ", googlePlaces=" + places +
                '}';
    }
}
