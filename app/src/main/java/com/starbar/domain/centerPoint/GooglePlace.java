package com.starbar.domain.centerPoint;

import java.util.List;

public class GooglePlace {

    private String id;
    private Coordinates location;
    private String name;
    private Double rating;
    private List<String> types;

    public GooglePlace(String id, Coordinates location, String name, Double rating, List<String> types) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.rating = rating;
        this.types = types;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Coordinates getLocation() {
        return location;
    }

    public void setLocation(Coordinates location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "GooglePlace{" +
                "placeId='" + id + '\'' +
                ", location=" + location +
                ", placeName='" + name + '\'' +
                ", rating=" + rating +
                ", types=" + types +
                '}';
    }
}
